class Test {
    fun sum(a: Int, b: Int) : Int? {
        return a + b
    }

    fun sumInferredReturnTypeAndExpressionBody(a:Int, b:Int) = a + b

    fun justPrintSmth(s:String) : Unit {
        println(s)
    }

    fun maxOf(a:Int, b:Int) = if (a>b) a else b

    fun kotlinSwitch(obj:Any) : String {
        return when (obj) {
            is Int -> "Integer : $obj"
            is Double -> "Double : $obj"
            is String -> obj.toString()
            else -> "Hmmm?"
        }
    }

    fun isIn(obj: String) {
        val fruits = listOf("apple", "orange", "peach")
        if (obj in fruits) println("is In")
        println("is Out")
    }

    fun containsEven(collection: Collection<Int>): Boolean = collection.any { it -> it%2==0 }
}
