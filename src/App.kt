fun main() {
    println("Hello World from App!")

    val list = listOf("one", "two", "three")

    for (item in list) println(item)

    for (index in list.indices) println("item at $index is ${list[index]}")

    for (x in 1..5) println(x)

    for(x in 1..10 step 3) println(x)
    val test = Test()
    println(test.sum(2, 5))
    println(test.sumInferredReturnTypeAndExpressionBody(10, 8))
    println(test.maxOf(7, 9))
    test.justPrintSmth("hello hello")

    println(test.kotlinSwitch(true))

    test.isIn("apple")

    val animals = listOf("bear", "tiger", "bee", "beaver", "cat", "lion")
    animals.filter { it.endsWith("r") }
        .sortedByDescending {it}
        .map{it.toUpperCase()}
        .forEach{println(it)}

    println(test.containsEven(listOf(3,1, 9)))

    var s :String? = "coucou"
    s = null

}

