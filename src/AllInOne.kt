import kotlin.math.sqrt

fun main(){
    val circle= Circle()
    val rectangle= Rectangle(3.8, 5.0)
    val triangle= Triangle(3.7,9.7, 3.3)
    println(triangle.calculateArea())
    println(circle.calculateArea())
    println(rectangle.calculateArea())
}

abstract class Shape(val sides: List<Double>?) {
    val perimeter: Double get() {
        if (sides != null) {
            return sides.sum()
        }
        return 0.0
    }
    abstract fun calculateArea(): Double
}

data class Rectangle(
    var height: Double,
    var length: Double
) : Shape(listOf(height, length, height, length)) {
    override fun calculateArea(): Double = height * length
}

class Triangle(
    var sideA:Double,
    var sideB: Double,
    var sideC: Double
):Shape(listOf(sideA, sideB, sideC)) {
    override fun calculateArea(): Double {
        val s = perimeter / 2
        return sqrt(s * (s - sideA) * (s - sideB) * (s - sideC))
    }
}
class Circle():Shape(null){
    override fun calculateArea(): Double {
        val radius = 12
        return Math.PI * (radius*radius)
    }

}
